import request from '@/utils/request'
import store from '@/store'

export function getLog(params) {
  const logName = store.state.analysis.currentLog
  return request({
    url: `/mk2/${logName}`,
    method: 'get',
    params: params
  })
}

export function getNLogElements() {
  const logName = store.state.analysis.currentLog
  return request({
    url: `/mk2/${logName}/count`,
    method: 'get'
  })
}

export function searchLog (data, params) {
  const logName = store.state.analysis.currentLog
  return request({
    url: `/mk2/${logName}/search`,
    method: 'post',
    data: data,
    params: params
  })
}

export function searchNLogElements(data) {
  const logName = store.state.analysis.currentLog
  return request({
    url: `/mk2/${logName}/search/count`,
    method: 'post',
    data: data,
  })
}
