module.exports = {
  title: 'DyNa 2019',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: true,

  /**
   * @type {string}
   * @description API server base url
   */
  apiBaseURL: "http://localhost:8000"
}
