import axios from 'axios'
import store from '@/store'

// create an axios instance
const service = axios.create({
  baseURL: store.state.settings.apiBaseURL, // url = base url + request url
  timeout: 10000 // request timeout [ms]
})

// request interceptor
service.interceptors.request.use(
  config => {
    // do something before request is sent
    return config
  },
  error => {
    // do something with request error
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  response => {
    const res = response.data
    if (response.status !== 200) {
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
)

export default service
