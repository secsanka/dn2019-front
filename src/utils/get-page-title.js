import defaultSettings from '@/settings'

const title = defaultSettings.title || 'DyNa 2019'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
