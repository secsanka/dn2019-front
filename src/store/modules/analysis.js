const state = {
  logList: [],
  currentLog: '',
  key: 0
}

const mutations = {
  CHANGE_CURRENT_LOG: (state, logName) => {
    if(state.logList.includes(logName)){
      state.currentLog = logName
      state.key += 1
    }
  },
  APPEND_LOG: (state, logName) => {
    if(!state.logList.includes(logName)){
      state.logList.push(logName)
    }
  }
}

const actions = {
  changeCurrentLog({ commit }, logName) {
    commit('CHANGE_CURRENT_LOG', logName)
  },
  appendLog({ commit }, logName) {
    commit('APPEND_LOG', logName)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

