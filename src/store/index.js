import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import analysis from './modules/analysis'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    analysis
  },
  getters
})

export default store
