import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'ダッシュボード',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'ダッシュボード', icon: 'dashboard' }
    }]
  },
  {
    path: '/logtable',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/table/index'),
        name: 'ログテーブル',
        meta: { title: 'ログテーブル', icon: 'table' }
      }
    ]
  },
  {
    path: '/proctree',
    component: Layout,
    children: [
      {
        path: 'index',
        component: () => import('@/views/tree/index'),
        name: 'プロセスツリー',
        meta: { title: 'プロセスツリー', icon: 'tree' }
      }
    ]
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
