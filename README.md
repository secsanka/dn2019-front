# DyNa Dashboard
SolitonDataset解析用のいい感じのダッシュボード

![ロゴ](./src/assets/logo_facebook_cover_photo_2.png)

## Project setup
Node.jsのパッケージマネージャはnpmではなく、yarnを使用している。

```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### 参考サイト
以下のテンプレを改変しているので、改変元のソースを参考に機能を追加していけるかもしれない。
- [vue-element-admin](https://panjiachen.github.io/vue-element-admin-site/)
- [vue-admin-template](https://github.com/PanJiaChen/vue-admin-template)

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
